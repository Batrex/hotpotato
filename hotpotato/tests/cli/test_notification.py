import datetime

from hotpotato.notifications import alerts, handovers, messages
from hotpotato.tests import oncall_contacts, rotations, servers, users


def test_get(runner, session):
    """
    Test get runs without any errors.
    """
    user = users.UserFactory()
    session.commit()

    message = messages.create(tenant_id=None, body="test", to_user_id=user.id)
    result = runner.invoke(args=["notification", "get", str(message.id)])
    assert result.exit_code is 0


def test_alert(runner, session):
    """
    Test alert sends an alert.
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    server = servers.ServerFactory()
    session.commit()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    count = len(alerts.get_by(output="Something is probably broken"))
    result = runner.invoke(
        args=[
            "notification",
            "send",
            "alert",
            "0",
            str(user.id),
            "host",
            str(server.id),
            "TEST123",
            "example.com",
            "Example Wesbite",
            "Example Service",
            "CRITICAL",
            "Something is probably broken",
            timestamp,
        ]
    )
    assert result.exit_code is 0
    assert len(alerts.get_by(output="Something is probably broken")) > count


def test_handover(runner, session):
    """
    Test handover sends a handover notification.
    """
    rotation = rotations.RotationFactory()
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()

    rotation_id = rotation.id
    user_id = user.id

    count = len(handovers.get_by(rotation_id=rotation_id))
    result = runner.invoke(
        args=["notification", "send", "handover", "0", str(rotation_id), str(user_id)]
    )
    assert result.exit_code is 0
    assert len(handovers.get_by(rotation_id=rotation_id)) > count


def test_message(runner, session):
    """
    Test message sends a message.
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()

    user_id = user.id

    count = len(messages.get_by(user_id=user_id))
    result = runner.invoke(
        args=["notification", "send", "message", "0", "Test", str(user_id)]
    )
    assert result.exit_code is 0
    assert len(messages.get_by(user_id=user_id)) > count
