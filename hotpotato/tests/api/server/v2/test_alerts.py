"""
Test the V2 alert endpoints
"""

from http import HTTPStatus

import pytest

from hotpotato.notifications import alerts
from hotpotato.tests import notifications, rotations, servers, users


def test_get_invalid_alert(client, session):
    """
    Test getting an alert that doesn't exist returns 404.
    """
    server = servers.ServerFactory()
    session.commit()
    r = client.get(
        "/api/server/v2/alerts/get/5",
        headers={"Authorization": "apikey {}".format(server.apikey)},
    )

    assert r.status_code == HTTPStatus.NOT_FOUND


def test_get_valid_alert(client, session):
    """
    Test getting an alert that does exist returns 200 and the alert.
    """
    server = servers.ServerFactory()
    users.UserFactory()
    session.commit()
    alert = notifications.NotificationFactory(notif_type="alert")
    session.commit()

    r = client.get(
        "/api/server/v2/alerts/get/{}".format(alert.id),
        headers={"Authorization": "apikey {}".format(server.apikey)},
    )

    assert r.status_code == HTTPStatus.OK
    assert r.json["data"]["id"] == alert.id
    assert r.json["data"]["output"] == alert.json["output"]


def test_add_notification_empty_request(client, session):
    """
    Test adding a notification with an empty request returns 422.
    """
    server = servers.ServerFactory()
    rotations.RotationFactory()
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={},
        headers={"Authorization": "apikey {}".format(server.apikey)},
    )

    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_add_notification_success(client, session):
    """
    Test adding a valid notification creates the notification and returns 201.
    """
    server = servers.ServerFactory()
    rotations.RotationFactory()
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "alert_type": "service",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "service_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": "2018-11-22T11:18:22+0000",
        },
        headers={"Authorization": "apikey {}".format(server.apikey)},
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.json["data"]["id"]))

    assert alert.json["output"] == "bob"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_add_notification_host_success(client, session):
    """
    Test adding a valid host notification creates the notification and
    returns 201.
    """
    server = servers.ServerFactory()
    rotations.RotationFactory()
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "alert_type": "host",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": "2018-11-22T11:18:22+0000",
        },
        headers={"Authorization": "apikey {}".format(server.apikey)},
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.json["data"]["id"]))

    assert alert.json["output"] == "bob"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ("2018-11-22 11:18:22+0000", "2018-11-22 11:18:22"),  # With space instead of T
        ("2018-11-22T11:18:22+0000", "2018-11-22 11:18:22"),  # Regular iso8601
        (
            "2018-11-22 11:18:22",
            "2018-11-21 22:18:22",
        ),  # No timezone form servers timezone
    ],
)
def test_add_notification_dateformating(client, session, test_input, expected):
    """
    Test adding a valid notification with various timestamp formats to check
    that it is added correctly and returns 201.
    """
    server = servers.ServerFactory(timezone="Pacific/Auckland")
    rotations.RotationFactory()
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "alert_type": "service",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "service_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": test_input,
        },
        headers={"Authorization": "apikey {}".format(server.apikey)},
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.json["data"]["id"]))

    assert alert.json["output"] == "bob"
    assert alert.json["timestamp"] == expected
