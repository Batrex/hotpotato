"""
Alert classes and functions.
"""


from datetime import datetime, timezone as dt_timezone

from hotpotato import users, util
from hotpotato.models import Alert
from hotpotato.notifications import notifications

NOTIF_TYPE = "alert"

# A frozen set of values representing all possible JSON API parameters for an alert.
JSON_API_PARAMS = frozenset(
    (
        "wr",
        "alert_type",
        "server_id",
        "trouble_code",
        "hostname",
        "display_name",
        "service_name",
        "state",
        "output",
        "timestamp",
    )
)


# A frozen set of values representing the type of an alert.
TYPE = frozenset(("service", "host"))

# A frozen set of values representing the state of a service alert.
SERVICE_STATE = frozenset(("OK", "WARNING", "CRITICAL"))

# A frozen set of values representing the state of a host alert.
HOST_STATE = frozenset(("UP", "DOWN"))

# A frozen set of values representing all possible states of a alert.
STATE = SERVICE_STATE | HOST_STATE


def get(ale_id):
    """
    Get an alert.
    """

    return notifications.get(ale_id)


def get_by(**kwargs):
    """
    Return a tuple of all alerts matching the given search parameters.
    """

    kwargs["notif_type"] = "alert"
    return notifications.get_by_helper(**kwargs)


def get_query(ale_id=None):
    """
    Get a query object for selecting the row object for an alert.
    If ale_id is specified, applies a filter for it on the query object.
    """

    return notifications.get_query(ale_id).filter_by(notif_type="alert")


def get_failed_query():
    """
    Return a query object for getting failed alerts.
    """

    return get_query().filter(notifications.in_json("status", "SEND_FAILED"))


def get_failed():
    """
    Return a list of all Alert that failed to send.
    """

    return (obj for obj in get_failed_query().all())


def get_num_failed():
    """
    Get the number of failed alerts.
    """

    return get_failed_query().count()


# pylint: disable=too-many-arguments
def create(
    tenant_id,
    user_id,
    alert_type,
    server,
    trouble_code,
    hostname,
    display_name,
    service_name,
    state,
    output,
    timestamp,
):
    """
    Create a new alert.
    """

    # pylint: disable=too-many-locals

    user = users.get(user_id)

    if not trouble_code:
        trouble_code = "NOTC"

    ts_str = util.datetime_get_as_string(timestamp)
    user_ts_str = datetime.strftime(
        timestamp.replace(tzinfo=dt_timezone.utc).astimezone(user.pytz_timezone),
        "%Y-%m-%d %H:%M:%S",
    )

    json_obj = {
        "alert_type": alert_type,
        "server_id": server.id,
        "trouble_code": trouble_code,
        "hostname": hostname,
        "display_name": display_name,
        "service_name": service_name,
        "state": state,
        "output": output,
        "timestamp": ts_str,
    }

    # Generate the alert body using the JSON object data and user data.
    #
    # There are two different types of alerts:
    # * service - used for reporting the state of services on hosts
    # * host    - used for reporting the status of the hosts themselves
    #
    # Any other alert type is unsupported and will raise a ValueError.
    if alert_type == "service":
        body = (
            "{server.hostname}: {trouble_code} {hostname} {service_name} state is "
            "{state} ({output}) "
            "{user_timestamp}"
        ).format(**json_obj, server=server, user_timestamp=user_ts_str)
    elif alert_type == "host":
        body = (
            "{server.hostname}: {trouble_code} {hostname} state is "
            "{state} ({output}) "
            "{user_timestamp}"
        ).format(**json_obj, server=server, user_timestamp=user_ts_str)
    else:
        raise ValueError(
            'Unexpected value "{}" for alert_type, '
            'must be either "service" or "host"'.format(alert_type)
        )

    return notifications.create(
        tenant_id=tenant_id,
        notif_model=Alert,
        body=body,
        user_id=user_id,
        json_obj=json_obj,
    )
