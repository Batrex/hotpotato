"""
Notification exception classes.
"""


from hotpotato import notifications


class NotificationError(Exception):
    """
    Notification exception base class.
    """

    pass


class NotificationCreateError(NotificationError):
    """
    Exception while trying to create a notification.
    """

    pass


class NotificationSendError(NotificationError):
    """
    Exception while trying to send a notification.
    """

    pass


class NotificationIDError(NotificationError):
    """
    Exception for an invalid or non-existent notification ID.
    """

    def __init__(self, notif_id):
        self.notif_id = notif_id
        super().__init__(
            "Invalid notification ID {} (or, notification does not exist)".format(
                self.notif_id
            )
        )


class NotificationValueError(NotificationError):
    """
    Exception for an invalid notification value.
    """

    # pylint: disable=too-many-arguments
    def __init__(self, key, actual_value, notif_id, expected_value, possible_values):
        self.actual_value = actual_value
        self.notif_id = notif_id
        self.expected_value = expected_value
        self.possible_values = possible_values
        msg = ""
        if notif_id:
            msg += "Invalid {} for notification with ID {}: ".format(key, notif_id)
        else:
            msg += "Invalid {} for new notification: ".format(key)
        if expected_value:
            msg += "Expected {}, ".format(expected_value)
        else:
            msg += "Expected one of [{}], ".format(", ".join(possible_values))
        msg += "got {}".format(actual_value)
        super().__init__(msg)


class NotificationTypeError(NotificationValueError):
    """
    Exception for an invalid notification type.
    """

    def __init__(
        self,
        actual_value,
        notif_id=None,
        expected_value=None,
        possible_values=notifications.TYPE,
    ):
        super().__init__(
            "type", actual_value, notif_id, expected_value, possible_values
        )


class NotificationStatusError(NotificationValueError):
    """
    Exception for an invalid notification status.
    """

    def __init__(
        self,
        actual_value,
        notif_id=None,
        expected_value=None,
        possible_values=notifications.STATUS,
    ):
        super().__init__(
            "status", actual_value, notif_id, expected_value, possible_values
        )


class NotificationMethodError(NotificationValueError):
    """
    Exception for an invalid notification method.
    """

    def __init__(
        self,
        actual_value,
        notif_id=None,
        expected_value=None,
        possible_values=notifications.METHOD,
    ):
        super().__init__(
            "method", actual_value, notif_id, expected_value, possible_values
        )


class NotificationProviderError(NotificationValueError):
    """
    Exception for an invalid notification provider.
    """

    def __init__(
        self,
        actual_value,
        notif_id=None,
        expected_value=None,
        possible_values=notifications.PROVIDER,
    ):
        super().__init__(
            "provider", actual_value, notif_id, expected_value, possible_values
        )


class NotificationProviderNotificationIDError(NotificationError):
    """
    Exception for an invalid provider notification ID.
    """

    def __init__(self, provider, provider_notif_id):
        self.provider = provider
        self.provider_notif_id = provider_notif_id
        super().__init__(
            "Invalid provider notification ID (or provider notification ID does not exist): "
            "{} (provider: {})".format(provider_notif_id, provider)
        )


class NotificationProviderNotificationIDTypeError(NotificationError):
    """
    Exception for an invalid provider notification ID.
    """

    def __init__(self, provider_notif_id, notif_id=None):
        self.provider_notif_id = provider_notif_id
        self.notif_id = notif_id

        err_message = "Invalid provider notification ID type"
        if self.notif_id:
            err_message += " on notification with ID {}".format(self.notif_id)
        err_message += ": got {}, expected str".format(self.provider_notif_id)

        super().__init__(err_message)
