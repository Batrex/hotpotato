"""
views.py contains all the routes for Hot Potato
"""


import collections
from datetime import date, datetime, timedelta

import dateutil
import flask_security
import pkg_resources
import pytz
import requests
import werkzeug.datastructures
from flask import (
    current_app,
    flash,
    jsonify,
    make_response,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_security import current_user, login_required, roles_accepted

from hotpotato import (
    heartbeats,
    oncall_contacts,
    servers as servers_module,
    users,
    util,
)
from hotpotato.api.functions import (
    exceptions as api_functions_exceptions,
    notifications as api_functions_notifications,
)
from hotpotato.api.server import key as api_server_key
from hotpotato.api.web.v1.notifications import handlers as api_notification_handlers
from hotpotato.models import Rotation, Server, User, db
from hotpotato.notifications import (
    alerts,
    exceptions as notifications_exceptions,
    handovers,
    messages,
    notifications,
)
from hotpotato.servers import ServerCreateError, create as create_server
from .functions import to_local_time
from .proxies import get_current_pager_person
from .servers import get_missed_hbeats as get_missed_hbeats_servs, get_num_missed_hbeats

NULL_USER = User(name="No one")


@login_required
def main_site():
    """
    The main index of the API.
    """

    return render_template(
        "main.html",
        pagerperson=get_current_pager_person(rotation="sysadmins") or NULL_USER,
        num_failed_ales=alerts.get_num_failed(),
    )


@login_required
def get_servers():
    """
    Page that displays information about the servers that talk to Hot Potato.
    """

    subquery_last_hbeats = heartbeats.get_last_query().subquery()
    servs = (
        db.session.query(
            Server.hostname,
            Server.timezone,
            Server.enable_heartbeat,
            Server.disable_missed_heartbeat_notif,
            subquery_last_hbeats.c.last_hbeat,
            Server.id,
            Server.link,
        )
        .outerjoin(subquery_last_hbeats, subquery_last_hbeats.c.server_id == Server.id)
        .all()
    )

    return render_template(
        "list_servers.html",
        now=datetime.utcnow(),
        servs=servs,
        num_missed_hbeats=get_num_missed_hbeats(),
    )


# pylint: disable=inconsistent-return-statements
@login_required
def add_server():
    """
    Add a new monitoring server. This generates and returns an API key to the user.
    """

    if request.method == "GET":
        return render_template("create_server.html", timezones=pytz.common_timezones)

    if request.method == "POST":
        f = request.form
        apikey = api_server_key.create()
        enable_heartbeat = f.get("enable_heartbeat") is not None
        disable_heartbeat_notif = f.get("disable_missed_heartbeat_notif") is not None

        try:
            create_server(
                f["hostname"],
                apikey,
                f["timezone"],
                f["link"],
                enable_heartbeat=enable_heartbeat,
                disable_missed_heartbeat_notif=disable_heartbeat_notif,
                missed_heartbeat_limit=f["missed_heartbeat_limit"],
            )

            return render_template("server_created.html", apikey=apikey)

        except ServerCreateError as err:
            flash("{}.".format(str(err)), "error")
            return render_template(
                "create_server.html", timezones=pytz.common_timezones
            )


@login_required
def get_servers_missed_hbeats():
    """
    Page that displays information about servers that have paged due to
    the server not having received enough heartbeats, or no heartbeats at all.
    """

    servs = get_missed_hbeats_servs().values()

    return render_template(
        "list_servers_missed_hbeats.html", servs=servs, num_missed_hbeats=len(servs)
    )


@roles_accepted("admin")
def server_toggle_heartbeat(server_id):
    """
    Toggle whether heartbeats are enabled for a server.
    """

    server = Server.query.filter_by(id=server_id).first()
    server.enable_heartbeat = not server.enable_heartbeat
    db.session.commit()

    return redirect(url_for("get_servers"))


@roles_accepted("admin")
def server_toggle_missed_heartbeat(server_id):
    """
    Toggle disabling missed heartbeat notifications for a server.
    """

    server = Server.query.filter_by(id=server_id).first()

    server.disable_missed_heartbeat_notif = not server.disable_missed_heartbeat_notif
    db.session.commit()

    return redirect(url_for("get_servers"))


def render_notifications_page(
    heading="Notifications", handler_name="notifications", json_attr_info=None
):
    """
    Display all notifications, with optional custom heading and JSON attributes.
    """

    handler = api_notification_handlers.get(handler_name)

    # The list of possible search filter attributes, in the order they are meant
    # to show on the web page (using an OrderedDict).
    attr_info_list = [
        ("start_date", {"description": "Start Date", "type": "date"}),
        ("end_date", {"description": "End Date", "type": "date"}),
        ("id", {"description": "Notification ID", "type": "int"}),
        ("tenant_id", {"description": "Tenant ID", "type": "int"}),
        ("user_id", {"description": "User ID", "type": "int"}),
        ("received_dt", {"description": "Received Date/Time", "type": "datetime"}),
    ]

    if handler.NOTIF_TYPE == "notification":
        attr_info_list.append(
            (
                "notif_type",
                {
                    "description": "Notification Type",
                    "notif_type": "enum",
                    "values": notifications.TYPE,
                },
            )
        )

    attr_info_list.extend(
        (
            ("body", {"description": "Notification Body", "type": "str"}),
            ("version", {"description": "Notification Version", "type": "int"}),
            ("node_name", {"description": "Handling Node Name", "type": "str"}),
            ("event_id", {"description": "Associated Event ID", "type": "int"}),
            ("ticket_id", {"description": "Associated Ticket ID", "type": "str"}),
            (
                "status",
                {
                    "description": "Sending Status",
                    "type": "enum",
                    "values": notifications.STATUS,
                },
            ),
            (
                "method",
                {
                    "description": "Sending Method",
                    "type": "enum",
                    "values": notifications.METHOD,
                },
            ),
            (
                "provider",
                {
                    "description": "Sending Provider",
                    "type": "enum",
                    "values": notifications.PROVIDER,
                },
            ),
            (
                "provider_notif_id",
                {"description": "Provider Notification ID", "type": "str"},
            ),
        )
    )
    search_filter_attr_info = collections.OrderedDict(attr_info_list)

    if json_attr_info:
        search_filter_attr_info.update(json_attr_info)

    # Get the search filters.
    search_filters = api_functions_notifications.search_filters_get(
        handler=handler, fail_if_empty=False
    )
    if not search_filters:
        now_dt = util.datetime_process(datetime.utcnow(), as_utc=True, to_user_tz=True)
        start_date = now_dt.date()  # Inclusive
        end_date = start_date + timedelta(days=1)  # Exclusive
        search_filters = werkzeug.datastructures.ImmutableMultiDict(
            [("start_date", start_date.isoformat()), ("end_date", end_date.isoformat())]
        )

    # Query the database according to the search filters, flashing any error message
    # that results if the query fails.
    try:
        id_notif = collections.OrderedDict(
            (
                (notif.id, notif)
                for notif in api_functions_notifications.get_from_search_filters(
                    handler, search_filters, eager=True
                )
            )
        )
    except api_functions_exceptions.APISearchFilterError as err:
        flash("{}.".format(str(err)), "error")
        id_notif = {}

    notif_servers = {}
    notif_users = {}
    reply_notifs = {}

    for notif in id_notif.values():
        if notif.notif_type == "alert":
            if notif.json["server_id"] not in notif_servers:
                notif_servers[notif.json["server_id"]] = servers_module.get(
                    notif.json["server_id"]
                )
        elif notif.notif_type == "message":
            if (
                notif.json["reply_to_notif_id"]
                and notif.json["reply_to_notif_id"] not in reply_notifs
            ):
                reply_notifs[notif.json["reply_to_notif_id"]] = notifications.get(
                    notif.json["reply_to_notif_id"]
                )
        elif notif.notif_type == "handover":
            if notif.json["to_user_id"] and notif.json["to_user_id"] not in notif_users:
                notif_users[notif.json["to_user_id"]] = users.get(
                    notif.json["to_user_id"]
                )

        if notif.notif_type == "handover" or notif.notif_type == "message":
            if (
                notif.json["from_user_id"]
                and notif.json["from_user_id"] not in notif_users
            ):
                notif_users[notif.json["from_user_id"]] = users.get(
                    notif.json["from_user_id"]
                )

    return render_template(
        "notifications/get.html",
        heading=heading,
        current_search_filters=search_filters,
        search_filter_attr_info=search_filter_attr_info,
        id_notif=id_notif,
        notif_servers=notif_servers,
        users=notif_users,
        reply_notifs=reply_notifs,
    )


@login_required
def notifications_get():
    """
    Display all notifications.
    """

    return render_notifications_page()


@login_required
def notifications_get_by_id(notif_id):
    """
    Display information about a specific notifications.
    """

    try:
        return render_template(
            "notifications/get_by_id.html", notif=notifications.get(notif_id)
        )

    except notifications_exceptions.NotificationIDError as err:
        return make_response(str(err), 404)


@login_required
def notifications_messages_get():
    """
    Display all messages.
    """

    return render_notifications_page(
        heading="Messages",
        handler_name="messages",
        json_attr_info=collections.OrderedDict(
            (("from_user_id", {"description": "From User ID", "type": "int"}),)
        ),
    )


@login_required
def notifications_messages_get_by_id(notif_id):
    """
    Display information about a specific message.
    """

    try:
        return render_template(
            "notifications/messages/get_by_id.html", notif=messages.get(notif_id)
        )

    except notifications_exceptions.NotificationIDError as err:
        return make_response(str(err), 404)

    except notifications_exceptions.NotificationTypeError as err:
        return make_response(str(err), 415)


@login_required
def notifications_handovers_get():
    """
    Display all handover notifications.
    """

    return render_notifications_page(
        heading="Handovers",
        handler_name="handovers",
        json_attr_info=collections.OrderedDict(
            (
                ("rotation_id", {"description": "Rotation ID", "type": "int"}),
                ("to_user_id", {"description": "To User ID", "type": "int"}),
                ("from_user_id", {"description": "From User ID", "type": "int"}),
                ("message", {"description": "Handover Message", "type": "str"}),
            )
        ),
    )


@login_required
def notifications_handovers_get_by_id(notif_id):
    """
    Display information about a specific handover notification.
    """

    try:
        return render_template(
            "notifications/handovers/get_by_id.html", notif=handovers.get(notif_id)
        )

    except notifications_exceptions.NotificationIDError as err:
        return make_response(str(err), 404)

    except notifications_exceptions.NotificationTypeError as err:
        return make_response(str(err), 415)


@login_required
def notifications_alerts_get():
    """
    Display all alerts.
    """

    return render_notifications_page(
        heading="Alerts",
        handler_name="alerts",
        json_attr_info=collections.OrderedDict(
            (
                (
                    "alert_type",
                    {
                        "description": "Alert Type",
                        "type": "enum",
                        "values": alerts.TYPE,
                    },
                ),
                ("server_id", {"description": "Monitoring Server ID", "type": "int"}),
                ("trouble_code", {"description": "Trouble Code", "type": "str"}),
                (
                    "hostname",
                    {"description": "Hostname (of notifying host)", "type": "str"},
                ),
                ("service_name", {"description": "Service Name", "type": "str"}),
                (
                    "state",
                    {
                        "description": "Host/Service State",
                        "type": "enum",
                        "values": alerts.STATE,
                    },
                ),
                ("output", {"description": "Host/Service Output", "type": "str"}),
                (
                    "timestamp",
                    {
                        "description": "Timestamp (from monitoring server)",
                        "type": "datetime",
                    },
                ),
            )
        ),
    )


@login_required
def notifications_alerts_get_by_id(notif_id):
    """
    Display information about a specific alert.
    """

    try:
        return render_template(
            "notifications/alerts/get_by_id.html", notif=alerts.get(notif_id)
        )

    except notifications_exceptions.NotificationIDError as err:
        return make_response(str(err), 404)

    except notifications_exceptions.NotificationTypeError as err:
        return make_response(str(err), 415)


# TODO: Deprecated, will be removed in 0.8.5.
@login_required
def get_pages():
    """
    Display all alerts.
    """

    end_date_dt = dateutil.parser.isoparse(
        request.args.get("end_date", default=date.today().isoformat())
    )
    end_date = end_date_dt.date().isoformat()

    start_date = request.args.get(
        "start_date", default=(end_date_dt - timedelta(days=1)).date().isoformat()
    )

    show_warning = bool(request.args.get("show_warning", default=False))

    search_filters = werkzeug.datastructures.MultiDict(
        {("start_date", start_date), ("end_date", end_date)}
    )
    if not show_warning:
        search_filters.add("not_state", "WARNING")

    ales = tuple(
        alerts.Alert(obj=ale)
        for ale in api_functions_notifications.get_from_search_filters(
            api_notification_handlers.get("alerts"), search_filters
        )
    )

    return render_template(
        "list_pages.html",
        num_failed_ales=alerts.get_num_failed(),
        start_date=start_date,
        end_date=end_date,
        show_warning=show_warning,
        ales=ales,
    )


@login_required
def get_pages_failed():
    """
    Display the failed alerts.
    """

    failed_ales_query = alerts.get_failed_query()

    return render_template(
        "list_pages.html",
        ales=(alerts.Alert(obj=ale) for ale in failed_ales_query.all()),
        num_failed_ales=failed_ales_query.count(),
    )


@login_required
def get_page_details(ale_id):
    """
    Display information about a specific page.
    """

    try:
        return render_template("detail_pages.html", ale=alerts.get(ale_id))

    except notifications_exceptions.NotificationIDError as err:
        return make_response(str(err), 404)

    except notifications_exceptions.NotificationTypeError as err:
        return make_response(str(err), 415)


@login_required
def get_page_json(ale_id):
    """
    Return information about a specific page in JSON.
    """

    try:
        ale = alerts.get(ale_id)

        return make_response(
            jsonify(
                {
                    "id": ale.id,
                    "notificationtype": ale.json["alert_type"],
                    "source": ale.json["source_name"],
                    "hostname": ale.json["hostname"],
                    "displayname": ale.json["display_name"],
                    "state": ale.json["state"],
                    "servicename": ale.json["service_name"],
                    "output": ale.json["output"],
                    "icingadatetime": ale.json["timestamp"],
                    "acked": ale.json["status"] == "SEND_FAILED_ACKNOWLEDGED",
                    "modica_id": ale.json["provider_notif_id"],
                    "notified": ale.has_been_sent(),
                    "notification_author": None,
                    "notification_comment": None,
                }
            ),
            200,
        )

    except notifications_exceptions.NotificationIDError as err:
        return make_response(str(err), 404)

    except notifications_exceptions.NotificationTypeError as err:
        return make_response(str(err), 415)


@login_required
def ack_pages():
    """
    Endpoint for acknowledging alerts that failed to send.
    """

    for ale_id_str in request.form:
        ale_id = int(ale_id_str)

        try:
            ale = alerts.get(ale_id)

            if ale.json["status"] == "SEND_FAILED":
                current_app.logger.info(
                    "User {} marking failed alert with ID {} as acknowledged".format(
                        current_user.name, ale_id
                    )
                )
                ale.json["status"] = "SEND_FAILED_ACKNOWLEDGED"

        except notifications_exceptions.NotificationIDError as err:
            db.session.rollback()
            return make_response(str(err), 404)

        except notifications_exceptions.NotificationTypeError as err:
            db.session.rollback()
            return make_response(str(err), 415)

    # Only commit the acknowledgements after all have been successfully done.
    db.session.commit()

    return redirect(url_for("get_pages"))


# pylint: disable=inconsistent-return-statements
@login_required
def manage_account():
    """
    Page to view account information
    """

    if request.method == "GET":
        user = users.get_by_email(current_user.email)

        verifiable_contacts = tuple(
            oncall_contacts.get_for_user(current_user, verifiable=True)
        )
        unverifiable_contacts = tuple(
            oncall_contacts.get_for_user(current_user, verifiable=False)
        )

        all_timezones = pytz.all_timezones
        current_utc_date = datetime.utcnow()

        return render_template(
            "account/settings.html",
            user=user,
            verifiable_contacts=verifiable_contacts,
            unverifiable_contacts=unverifiable_contacts,
            all_timezones=all_timezones,
            date=current_utc_date,
        )

    # This should probably get its own endpoint like add_contact_method.
    if request.method == "POST":
        for item in request.form:
            print("Attempting to delete contact method with ID: {0}".format(item))
            oncall_contacts.delete_by_id(int(item))
        return redirect(url_for("manage_account"))


@login_required
def change_timezone():
    """
    Changes the preffered timezone of the user
    """

    person = User.query.filter_by(id=current_user.id).first()
    person.timezone = request.form["timezone"]
    print(
        "Setting TZ for user {0} ({1}) to {2}".format(
            person.id, person.name, person.timezone
        )
    )
    db.session.commit()
    return redirect(url_for("manage_account"))


# pylint: disable=inconsistent-return-statements
@login_required
def add_contact_method():
    """
    Page to allow adding contact methods for on-call people.
    """

    post_failed = False

    if request.method == "GET":
        return render_template(
            "account/add_contact_method.html",
            oncall_contact_methods=oncall_contacts.METHOD,
        )

    if request.method == "POST":
        if "method" not in request.form or not request.form["method"]:
            flash("Please choose a sending method.", "info")
            post_failed = True
            method = None
        else:
            method = request.form["method"]

        if "contact" not in request.form or not request.form["contact"]:
            flash("Please specify a contact detail.", "info")
            post_failed = True
            contact = None
        else:
            contact = request.form["contact"]

        if "priority" not in request.form or not request.form["priority"]:
            flash("Please specify a priority number.", "info")
            post_failed = True
            priority = None
        else:
            priority = request.form["priority"]

        send_pages = True if request.form.get("send_pages") else False
        send_failures = True if request.form.get("send_failures") else False

        verifiable = oncall_contacts.method_is_verifiable(method)

        # Check if the contact method already exists for the user.
        # If so, render a different page showing a warning for a duplicate contact.
        if oncall_contacts.exists(
            user_id=current_user.id, contact=contact, method=method
        ):
            flash("Contact method already exists.", "error")
            post_failed = True

        if oncall_contacts.exists(
            user_id=current_user.id, verifiable=verifiable, priority=priority
        ):
            flash(
                "{} contact method with the priority value {} already exists.".format(
                    "A verifiable" if verifiable else "An unverifiable", priority
                ),
                "error",
            )
            post_failed = True

        # Verify that the token is valid
        if method == "pushover":
            token = current_app.config["PUSHOVER_API_TOKEN"]
            verify = requests.post(
                "https://api.pushover.net/1/users/validate.json",
                json={"token": token, "user": contact},
            )
            if verify.status_code != 200 or verify.json()["status"] != 1:
                flash("Pushover token is invalid.", "error")
                post_failed = True

        if post_failed:
            return render_template(
                "account/add_contact_method.html",
                oncall_contact_methods=oncall_contacts.METHOD,
                method=method,
                contact=contact,
                priority=priority,
            )

        # If the contact method doesn't exist, create it.
        oncall_contact = oncall_contacts.create(
            user_id=current_user.id,
            contact=contact,
            method=method,
            priority=priority,
            send_pages=send_pages,
            send_failures=send_failures,
        )

        # Send a test message if the contact method is SMS.
        if method == "sms" or method == "pushover":
            tenant_id = None  # TODO: tenants

            messages.create(
                tenant_id=tenant_id,
                body="This is a test message",
                to_user_id=current_user.id,
            ).send(contact=oncall_contact)

        return redirect(url_for("manage_account"))


@login_required
def toggle_send_pages(contact_id):
    """
    Toggles the sending of pages to a contact method
    """
    oncall_contact = oncall_contacts.get(contact_id)
    if oncall_contact.send_pages:
        oncall_contact.send_pages = False
        db.session.commit()
    else:
        oncall_contact.send_pages = True
        db.session.commit()

    return redirect(url_for("manage_account"))


@login_required
def toggle_notify_on_fail(contact_id):
    """
    Toggle sending failure alerts to a contact method.
    """

    tenant_id = None  # TODO: tenants

    oncall_contact = oncall_contacts.get(contact_id)

    if oncall_contact.send_failures:
        current_app.logger.info(
            "Removing failure alerts for {}".format(current_user.name)
        )

        # Send an SMS before disabling the notifications.
        if oncall_contact.method == "sms":
            messages.create(
                tenant_id=tenant_id,
                body="You will no longer recieve failure alerts",
                to_user_id=current_user.id,
            ).send(contact=oncall_contact)

        oncall_contact.send_failures = False
        db.session.commit()

    else:
        current_app.logger.info(
            "Adding failure alerts for {}".format(current_user.name)
        )

        oncall_contact.send_failures = True
        db.session.commit()

        # Send a SMS notifying the user they will get failure notifications on this
        # on-call contact.
        if oncall_contact.method == "sms":
            messages.create(
                tenant_id=tenant_id,
                body="You will now get failure alerts",
                to_user_id=current_user.id,
            ).send(contact=oncall_contact)
    return redirect(url_for("manage_account"))


@login_required
def send_test_message(contact_id):
    """
    Sends a test message to a contact method.
    """
    tenant_id = None
    oncall_contact = oncall_contacts.get(contact_id)
    messages.create(
        tenant_id=tenant_id, body="This is a test message", to_user_id=current_user.id
    ).send(contact=oncall_contact)

    flash("A test message has been sent to {}".format(oncall_contact.contact), "info")

    return redirect(url_for("manage_account"))


# pylint: disable=inconsistent-return-statements
@roles_accepted("admin", "pagerpeeps")
def do_handover():
    """
    Endpoint to do pager handover.
    """

    if request.method == "GET":
        # TODO: custom rotations
        return render_template(
            "handover.html",
            pagerperson=get_current_pager_person(rotation="sysadmins") or NULL_USER,
        )

    if request.method == "POST":

        tenant_id = None  # TODO: tenants
        from_user = get_current_pager_person(rotation="sysadmins")
        to_user = current_user

        # Get the message from the new on-call person.
        message = request.form["message"]

        # Update the currently listed on-call person.
        rotation = Rotation.query.filter_by(rotname="sysadmins").first()
        rotation.person = to_user.id
        db.session.commit()

        from_user_id = from_user.id if from_user else None

        # Send the handover notifications.
        to_handover, from_handover = handovers.create(
            tenant_id=tenant_id,
            rotation_id=rotation.id,
            to_user_id=to_user.id,
            from_user_id=from_user_id,
            message=message,
        )
        to_handover.send()
        if from_handover:
            from_handover.send()

        return render_template(
            "handover.html", pagerperson=to_user, pager_taken=True, message=message
        )


# pylint: disable=inconsistent-return-statements
@login_required
def send_message():
    """
    Endpoint to send a message to the pager person.
    """

    if request.method == "GET":
        return render_template(
            "send_message.html",
            pagerperson=get_current_pager_person(rotation="sysadmins") or NULL_USER,
        )

    if request.method == "POST":
        tenant_id = None  # TODO: tenants
        body = request.form["message"]
        from_user = current_user
        to_user = get_current_pager_person(
            rotation="sysadmins"
        )  # TODO: custom rotations

        messages.create(
            tenant_id=tenant_id,
            body=body,
            to_user_id=to_user.id,
            from_user_id=from_user.id,
        ).send()

        return render_template("send_message.html", pagerperson=to_user, message=body)


# Admin only view - Contact Methods
# For toggling of SMS/Voice/etc
# Not currently used for actually turning things off/on


@roles_accepted("admin")
def admin_contact_methods():
    """
    Admin page to display contact methods
    """
    return render_template(
        "admin/contact-methods.html", the_contact_methods=oncall_contacts.METHOD
    )


def init_app(app):
    """
    Initialise the Flask app with the Hot Potato views.
    """

    # Set the template and static file folders in the app to their correct values.
    app.template_folder = pkg_resources.resource_filename("hotpotato", "templates")
    app.static_folder = pkg_resources.resource_filename("hotpotato", "static")

    # Add additional variables and macros to the app's Jinja2 environment.
    app.jinja_env.globals.update(
        notifications=notifications,
        alerts=alerts,
        handovers=handovers,
        messages=messages,
        servers=servers_module,
        users=users,
        util=util,
        node_name=util.node_name,
        current_user=flask_security.current_user,
        to_local_time=to_local_time,
    )

    # Add hard-coded configuration values.
    app.config["SECURITY_MSG_USER_DOES_NOT_EXIST"] = (
        "Incorrect username or password",
        "error",
    )
    app.config["SECURITY_MSG_INVALID_PASSWORD"] = (
        "Incorrect username or password",
        "error",
    )

    # Add URL rules for all views.
    app.add_url_rule("/", view_func=main_site, methods=["GET"])

    app.add_url_rule("/servers", view_func=get_servers, methods=["GET", "POST"])
    app.add_url_rule("/servers/add", view_func=add_server, methods=["GET", "POST"])
    app.add_url_rule(
        "/servers/missed_hbeats", view_func=get_servers_missed_hbeats, methods=["GET"]
    )
    app.add_url_rule(
        "/servers/toggle-heartbeat/<server_id>",
        view_func=server_toggle_heartbeat,
        methods=["POST"],
    )
    app.add_url_rule(
        "/servers/toggle-missed-heartbeat-notif/<server_id>",
        view_func=server_toggle_missed_heartbeat,
        methods=["POST"],
    )

    app.add_url_rule(
        "/notifications", view_func=notifications_get, methods=["GET", "POST"]
    )
    app.add_url_rule(
        "/notifications/<notif_id>", view_func=notifications_get_by_id, methods=["GET"]
    )
    app.add_url_rule(
        "/notifications/alerts",
        view_func=notifications_alerts_get,
        methods=["GET", "POST"],
    )
    app.add_url_rule(
        "/notifications/alerts/<notif_id>",
        view_func=notifications_alerts_get_by_id,
        methods=["GET"],
    )
    app.add_url_rule(
        "/notifications/handovers",
        view_func=notifications_handovers_get,
        methods=["GET", "POST"],
    )
    app.add_url_rule(
        "/notifications/handovers/<notif_id>",
        view_func=notifications_handovers_get_by_id,
        methods=["GET"],
    )
    app.add_url_rule(
        "/notifications/messages",
        view_func=notifications_messages_get,
        methods=["GET", "POST"],
    )
    app.add_url_rule(
        "/notifications/messages/<notif_id>",
        view_func=notifications_messages_get_by_id,
        methods=["GET"],
    )

    app.add_url_rule(
        "/pages",  # TODO: Remove /pages[/*] endpoint in 1.0
        view_func=get_pages,
        methods=["GET", "POST"],
    )
    app.add_url_rule(
        "/pages/failed",  # TODO: Remove /pages[/*] endpoint in 1.0
        view_func=get_pages_failed,
        methods=["GET", "POST"],
    )
    app.add_url_rule(
        "/pages/<ale_id>",  # TODO: Remove /pages[/*] endpoint in 1.0
        view_func=get_page_details,
        methods=["GET"],
    )
    app.add_url_rule(
        "/pages/<ale_id>/json",  # TODO: Remove /pages[/*] endpoint in 1.0
        view_func=get_page_json,
        methods=["GET", "POST"],
    )
    app.add_url_rule(
        "/pages/ack",  # TODO: Remove /pages[/*] endpoint in 1.0
        view_func=ack_pages,
        methods=["POST"],
    )

    app.add_url_rule("/account", view_func=manage_account, methods=["GET", "POST"])
    app.add_url_rule("/account/tz", view_func=change_timezone, methods=["POST"])
    app.add_url_rule(
        "/account/add-contact-method",
        view_func=add_contact_method,
        methods=["GET", "POST"],
    )
    app.add_url_rule(
        "/account/toggle-send-pages/<contact_id>",
        view_func=toggle_send_pages,
        methods=["POST"],
    )
    app.add_url_rule(
        "/account/toggle-send-failures/<contact_id>",
        view_func=toggle_notify_on_fail,
        methods=["POST"],
    )
    app.add_url_rule(
        "/account/send-test-message/<contact_id>",
        view_func=send_test_message,
        methods=["POST"],
    )
    app.add_url_rule("/handover", view_func=do_handover, methods=["GET", "POST"])
    app.add_url_rule("/message", view_func=send_message, methods=["GET", "POST"])
