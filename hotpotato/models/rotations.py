"""
Rotation model classes.
"""

from hotpotato.models.database import db
from hotpotato.models.users import User


class Rotation(db.Model):
    """
    Stores rotations and associated information.
    """

    __tablename__ = "rotations"

    id = db.Column(db.Integer, primary_key=True)
    rotname = db.Column(db.Text)
    person = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User, uselist=False)
