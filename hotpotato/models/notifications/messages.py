from hotpotato.models.notifications.notifications import Notification


class Message(Notification):
    """
    Message object class.
    """

    name = "message"
    description = "message"

    # pylint: disable=arguments-differ
    def send(self, run_async=True, contact=None, method=None):
        """
        Send the message.
        """

        # The reason why this import is here and not at the top of the file is
        # because when the actors get defined, they connect to the message queue broker
        # to register themselves as jobs to be run.
        # Therefore, the message queue broker needs to already be running BEFORE
        # the actors get imported/defined.
        from hotpotato.notifications.queue.actors import message as actor

        super().send(actor, run_async=run_async, contact=contact, method=method)

    __mapper_args__ = {"polymorphic_identity": "message"}
